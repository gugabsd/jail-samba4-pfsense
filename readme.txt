1. Criar imagem com uzip para a jail do samba
2. Imagem com arquivo .img formatado com zfs ? (ufs com acls?) -- ufs + acls
3. Pontos de montagem e configuração nessa imagem

# BITBUCKET
cd /path/to/my/repo
git remote add origin ssh://git@bitbucket.org/gugabsd/jail-samba4-pfsense.git
git push -u origin --all # pushes up the repo and its refs for the first time
git push -u origin --tags # pushes up any tags

# Construção da imagem:
makefs jail-samba4.img /usr/jails/release83
/usr/jails/release83/usr/bin/mkuzip -s 102400 jail-samba4.img


# Levanta imagem uzip do samba4
mdconfig -af /usr/jails/jail-samba4.img.uzip -u 10
mkdir /usr/samba4
mount -o ro /dev/md10.uzip /usr/samba4
mount -t devfs devfs /usr/samba4/dev

# if não existe, cria imagem:
#dd if=/dev/zero of=/usr/samba4-provision.img bs=1m count=500
mdconfig -af /usr/samba4-provision.img -u 11
#newfs -J /dev/md11
mount -o rw,acls /dev/md11 /usr/samba4/usr/samba4-provision
#zpool create -m /usr/samba4/usr/samba4-provision samba4-provision /dev/md11
#cp /etc/resolv.conf /usr/samba4/usr/samba4-provision/resolv.conf
#tar -C /usr/samba4/usr/samba4-provision/ -xJpf /usr/var.txz


#chroot /usr/samba4 /usr/local/bin/samba-tool domain provision \
#--use-ntvfs --domain=MUNDOUNIX --adminpass=Mudar123 --server-role=dc --realm=mundounix.com.br --targetdir=/usr/samba4-provision --configfile=/usr/samba4-provision/etc/smb4.conf

chroot /usr/samba4 /usr/local/bin/samba-tool domain provision \
--domain=MUNDOUNIX --adminpass=Mudar123 --server-role=dc --realm=mundounix.com.br --targetdir=/usr/samba4-provision --configfile=/usr/samba4-provision/etc/smb4.conf


chroot /usr/samba4 /usr/local/etc/rc.d/samba4 start


#!/bin/sh

start () {

	# Levanta imagem uzip do samba4
	mdconfig -af /usr/jails/jail-samba4.img.uzip -u 10
	mkdir /usr/samba4
	mount -o ro /dev/md10.uzip /usr/samba4
	mount -t devfs devfs /usr/samba4/dev
	
	if [ ! -f "/usr/samba4-provision.img" ]; then
		dd if=/dev/zero of=/usr/samba4-provision.img bs=1m count=50
		mdconfig -af /usr/samba4-provision.img -u 11
		newfs /dev/md11
		mount -o rw,acls /dev/md11 /usr/samba4/usr/samba4-provision
		cp /etc/resolv.conf /usr/samba4/usr/samba4-provision/resolv.conf
		tar -C /usr/samba4/usr/samba4-provision/ -xJpf /usr/var.txz
	else
		mdconfig -af /usr/samba4-provision.img -u 11
		mount -o rw,acls /dev/md11 /usr/samba4/usr/samba4-provision
	fi
}

$1
