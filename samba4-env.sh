#!/bin/sh

start () {

        # Levanta imagem uzip do samba4
        if ! df | grep md10.uzip; then
                mdconfig -af /usr/jails/jail-samba4.img.uzip -u 10
                if [ ! -d "/usr/samba4"]; then
                        mkdir /usr/samba4
                fi
                mount -o ro /dev/md10.uzip /usr/samba4
                mount -t devfs devfs /usr/samba4/dev

                if ! df | grep md11; then
                        if [ ! -f /usr/samba4-provision.img ]; then
                                dd if=/dev/zero of=/usr/samba4-provision.img bs=1m count=50
                                mdconfig -af /usr/samba4-provision.img -u 11
                                newfs /dev/md11
                                mount -o rw,acls /dev/md11 /usr/samba4/usr/samba4-provision
                                cp /etc/resolv.conf /usr/samba4/usr/samba4-provision/resolv.conf
                                tar -C /usr/samba4/usr/samba4-provision/ -xJpf /usr/var.txz
                        else
                                mdconfig -af /usr/samba4-provision.img -u 11
                                mount -o rw,acls /dev/md11 /usr/samba4/usr/samba4-provision
                        fi
                fi
        fi
        if [ -f /usr/samba4/usr/samba4-provision/etc/smb4.conf ]; then
                chroot /usr/samba4 /usr/local/etc/rc.d/samba4 start
        fi
}

$1

